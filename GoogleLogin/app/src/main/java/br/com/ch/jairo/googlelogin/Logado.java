package br.com.ch.jairo.googlelogin;

import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;


public class Logado extends AppCompatActivity {
    TextView nome,email;
    Button desloga;
    ImageView photo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logado);
        desloga=(Button)findViewById(R.id.deslogGoogle);
        nome=(TextView)findViewById(R.id.nome);
        email=(TextView)findViewById(R.id.email);
        photo=(ImageView)findViewById(R.id.photo);
        photo.setImageResource(R.drawable.boy);

        final Intent intent=getIntent();
        //Pega os dados que foram repassados pelo INTENT e adicionam eles aos textviews
        if(intent.getStringExtra("logadoCom").equals("go")){
            if (intent!=null){
                String name=intent.getStringExtra("nome_sobrenome");
                String emailString=intent.getStringExtra("email");
                nome.setText(name);
                this.email.setText(emailString);
                Picasso.get().load(intent.getStringExtra("url_photo")).into(photo);
            }
        }else if (intent.getStringExtra("logadoCom").equals("fb")){
            FirebaseUser user=FirebaseAuth.getInstance().getCurrentUser();
            if (user!=null){
                String nomef=user.getDisplayName();
                String emailf=user.getEmail();
                Log.i("idfa",user.getUid());
                nome.setText(nomef);
                this.email.setText(emailf);
                Log.i("idfa",user.getPhotoUrl().toString());
                Picasso.get().load(user.getPhotoUrl().toString()+"?width=300&height=300").into(photo);
            }
        }

        desloga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                if(intent.getStringExtra("logadoCom").equals("fb")){
                    LoginManager.getInstance().logOut();
                    startActivity(new Intent(Logado.this,MainActivity.class).putExtra("deslogado",false));
                    Log.i("teste","testef");
                }else {
                    startActivity(new Intent(Logado.this, MainActivity.class).putExtra("deslogado", true));
                    Log.i("teste","teste");
                }
                finish();
            }
        });



    }
}
